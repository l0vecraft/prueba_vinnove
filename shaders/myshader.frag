#version 460 core

#include <flutter/runtime_effect.glsl>

#define PI 3.14159265359
#define SCALE_FACTOR 8
#define TIME_SCALE 0.005

out vec4 fragColor;
uniform vec2 resolution;
uniform float time;

float normlizeTrigonometric(float value){
    return (value+1)/2;
}

void main(){
    vec2 pos = FlutterFragCoord().xy/resolution;
    float scaledTime = time*TIME_SCALE;
    float vertical = normlizeTrigonometric(sin(pos.x*PI*SCALE_FACTOR+scaledTime));
    float horizontal = normlizeTrigonometric(cos(pos.y*PI*SCALE_FACTOR+scaledTime));
    float diagonal = normlizeTrigonometric(sin((pos.x+pos.y)*PI*SCALE_FACTOR+scaledTime));

    vec3 verticalStripColor = vec3(1.0,0.0,0.0) *vertical;
    vec3 horizontalStripColor = vec3(0.0,1.0,0.0) *horizontal;
    vec3 diagonalStripColor = vec3(0.0,0.0,1.0) *diagonal;

    vec3 mixedColor = mix(mix(verticalStripColor,horizontalStripColor,pos.x),diagonalStripColor,pos.y);

    fragColor = vec4(mixedColor,1.0);
}