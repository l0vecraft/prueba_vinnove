#version 460 core

#include <flutter/runtime_effect.glsl>

#define SCALE_FACTOR 4

uniform vec2 resolution;
uniform float scrollPosition;
//-----------------

out vec4 fragColor;

const vec3 color1 =  vec3(166.,114.,196.)/255.;
const vec3 color2 =  vec3(199, 95, 251)/255.;
const vec3 color3 =  vec3(237, 51, 206)/255.;
const vec3 color4 =  vec3(7, 177, 187)/255.;
const vec3 color5 =  vec3(51, 111, 229)/255.;
const vec3 color6 =  vec3(223, 112, 244)/255.;

float normlizeTrigonometric(float value){
    return (value+1)/2;
}

vec3 getColor(vec2 position, float offset){
    int index = int(mod(floor(position.y+offset),4.0));
    if(index==0){
        return color1;
    } else if (index == 1) {
    return color2;
  } else if (index == 2) {
    return color3;
  } else {
    return color4;
  }
}
//? solo para tener esto en algun lado
vec3 gradientColors(vec2 pos, vec3 colors){
    vec3 color = 0.5 + 0.5*cos(scrollPosition+pos.xyx+colors);
    return color;
}

void main(){
  
    vec2 pos = FlutterFragCoord().xy/resolution;
    vec3 colors = vec3(0.0);
    // float steps =  pos.x+sin(pos.x*10+scrollPosition)*0.05;
    vec3 steps = 2+ 0.8*cos(scrollPosition+pos.xyx+vec3(0,2,4)*10);
    colors = mix(color1,color2,smoothstep(0.0,1.0,steps));
    colors = mix(colors,color3,smoothstep(1.0,2.0,steps));
    colors = mix(colors, color4,smoothstep(2.0,3.0, steps));
    colors = mix(colors, color5,smoothstep(3.0,4.0, steps));
    colors = mix(colors, color6,smoothstep(4.0,5.0, steps));
    

    fragColor = vec4(colors,1.0);
 
}
