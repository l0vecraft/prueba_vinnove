import 'package:go_router/go_router.dart';

import 'src/ui/screens/screens.dart';

class Routes {
  static final router = GoRouter(routes: [
    GoRoute(
        path: '/',
        builder: (context, state) => const HomeScreen(),
        routes: [
          GoRoute(
            path: 'todo',
            builder: (context, state) => const TodoScreen(),
          ),
          GoRoute(
            path: 'challenge',
            builder: (context, state) => const ChallengeScreen(),
          ),
          GoRoute(
            path: 'question',
            builder: (context, state) => const QuestionScreen(),
          ),
          GoRoute(
            path: 'result',
            builder: (context, state) => const ChallangeResultScreen(),
          ),
          GoRoute(
            path: 'notifications',
            builder: (context, state) => const NotificationScreen(),
          ),
          GoRoute(
            path: 'shaders',
            builder: (context, state) => const ShaderScreen(),
          )
        ]),
  ]);

  // switch (settings.name) {
  //   case '/':
  //     return MaterialPageRoute(
  //       builder: (context) => const HomeScreen(),
  //     );
  //   case 'todo':
  //     return MaterialPageRoute(
  //       builder: (context) => const TodoScreen(),
  //     );
  //   case 'challenge':
  //     return MaterialPageRoute(
  //       builder: (context) => const ChallengeScreen(),
  //     );
  //   case 'question':
  //     return MaterialPageRoute(
  //       builder: (context) => const QuestionScreen(),
  //     );
  //   case 'result':
  //     return MaterialPageRoute(
  //       builder: (context) => const ChallangeResultScreen(),
  //     );
  //   case 'notifications':
  //     return MaterialPageRoute(
  //       builder: (context) => const NotificationScreen(),
  //     );
  //   default:
  //     return MaterialPageRoute(
  //       builder: (context) => const HomeScreen(),
  //     );
  // }
}
