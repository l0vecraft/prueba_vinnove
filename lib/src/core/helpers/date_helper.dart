part of prueba_vinnove.helpers;

mixin DateHelper {
  List<NotificationSectionModel> sortNotifications(
      List<NotificationModel> notifications) {
    List<NotificationSectionModel> result = [];
    var today = <NotificationModel>[];
    var yesterday = <NotificationModel>[];
    var thisWeek = <NotificationModel>[];
    var thisMonth = <NotificationModel>[];
    var anothers = <NotificationModel>[];
    var actualDate = DateTime.now();
    notifications.forEach((notification) {
      if (DateUtils.isSameDay(actualDate, notification.createdAt)) {
        today.add(notification);
      } else if (DateUtils.isSameDay(
          actualDate.subtract(const Duration(days: 1)),
          notification.createdAt)) {
        yesterday.add(notification);
      } else if (_isSameWeek(notification.createdAt, actualDate)) {
        thisWeek.add(notification);
      } else if (DateUtils.isSameMonth(actualDate, notification.createdAt)) {
        thisMonth.add(notification);
      } else {
        anothers.add(notification);
      }
    });
    result = [
      if (today.isNotEmpty) ...[
        NotificationSectionModel(title: 'Hoy', notifications: today)
      ],
      if (today.isNotEmpty) ...[
        NotificationSectionModel(title: 'Ayer', notifications: yesterday)
      ],
      if (today.isNotEmpty) ...[
        NotificationSectionModel(title: 'Esta semana', notifications: thisWeek)
      ],
      if (today.isNotEmpty) ...[
        NotificationSectionModel(title: 'Este mes', notifications: thisMonth)
      ],
      if (today.isNotEmpty) ...[
        NotificationSectionModel(title: 'Anteriores', notifications: anothers)
      ],
    ];
    return result;
  }

  bool _isSameWeek(DateTime firstDate, DateTime secondDate) {
    var result = (secondDate.difference(firstDate).inDays / 7).ceil();
    return result <= 1 ? true : false;
  }
}
