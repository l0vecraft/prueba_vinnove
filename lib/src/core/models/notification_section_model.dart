part of prueba_vinnove.models;

class NotificationSectionModel {
  final String title;
  final List<NotificationModel> notifications;

  NotificationSectionModel({required this.title, required this.notifications});
}
