part of prueba_vinnove.models;

class TodoModel {
  TodoModel({required this.title, this.hasDone = false});

  String title;
  bool hasDone;
}
