part of prueba_vinnove.models;

class ChallengeOptions {
  final String title;
  final String subtitle;
  final String imagePath;
  final bool isBlocked;
  bool isSelected;

  ChallengeOptions({
    required this.title,
    required this.subtitle,
    required this.imagePath,
    this.isBlocked = false,
    this.isSelected = false,
  });
}

var listOfOptions = [
  ChallengeOptions(
      title: "Wellbeing",
      subtitle: "1 minute",
      imagePath: 'assets/images/cup.svg'),
  ChallengeOptions(
      title: "Self-Compassion",
      subtitle: "3 minutes",
      imagePath: 'assets/images/heart.svg'),
  ChallengeOptions(
      title: "Stress Temperature",
      subtitle: "3 minutes",
      imagePath: 'assets/images/stress.svg'),
  ChallengeOptions(
      title: "Self Awareness",
      subtitle: "2 minute",
      imagePath: 'assets/images/hands.svg',
      isBlocked: true),
];
