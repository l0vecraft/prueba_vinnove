part of prueba_vinnove.models;

class NotificationModel {
  final String title;
  final DateTime createdAt;

  NotificationModel({required this.title, required this.createdAt});
}

var listNotifications = [
  NotificationModel(title: 'notificacion 1', createdAt: DateTime.now()),
  NotificationModel(title: 'notificacion 2', createdAt: DateTime.now()),
  NotificationModel(
      title: 'notificacion 3',
      createdAt: DateTime.now().subtract(const Duration(days: 1))),
  NotificationModel(
      title: 'notificacion 4',
      createdAt: DateTime.now().subtract(const Duration(days: 1))),
  NotificationModel(
      title: 'notificacion 5',
      createdAt: DateTime.now().subtract(const Duration(days: 1))),
  NotificationModel(
      title: 'notificacion 6',
      createdAt: DateTime.now().subtract(const Duration(days: 2))),
  NotificationModel(
      title: 'notificacion 7',
      createdAt: DateTime.now().subtract(const Duration(days: 2))),
  NotificationModel(
      title: 'notificacion 8',
      createdAt: DateTime.now().subtract(const Duration(days: 10))),
  NotificationModel(
      title: 'notificacion 9',
      createdAt: DateTime.now().subtract(const Duration(days: 10))),
  NotificationModel(
      title: 'notificacion 10',
      createdAt: DateTime.now().subtract(const Duration(days: 10))),
];
