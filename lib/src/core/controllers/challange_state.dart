part of prueba_vinnove.controllers;

class ChallengeState {
  ChallengeState({
    required this.listOfOptions,
    this.question1,
    this.question2,
    this.question3,
    this.question4,
    this.question5,
  });

  final List<ChallengeOptions>? listOfOptions;
  final int? question1;
  final int? question2;
  final int? question3;
  final int? question4;
  final int? question5;

  int get result =>
      (question1! + question2! + question3! + question4! + question5!) * 4;
  bool get isEnabledContinueButton => (question1 != null &&
      question2 != null &&
      question3 != null &&
      question4 != null &&
      question5 != null);

  ChallengeState copyWith({
    List<ChallengeOptions>? listOfOptions,
    int? question1,
    int? question2,
    int? question3,
    int? question4,
    int? question5,
  }) =>
      ChallengeState(
        listOfOptions: listOfOptions ?? this.listOfOptions,
        question1: question1 ?? this.question1,
        question2: question2 ?? this.question2,
        question3: question3 ?? this.question3,
        question4: question4 ?? this.question4,
        question5: question5 ?? this.question5,
      );
}
