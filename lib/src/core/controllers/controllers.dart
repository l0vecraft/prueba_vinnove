library prueba_vinnove.controllers;

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:prueba_vinnove/src/core/helpers/helpers.dart';
import 'package:prueba_vinnove/src/core/models/models.dart';

part 'todo_controller.dart';
part 'todo_state.dart';

part 'challange_state.dart';
part 'challenge_controller.dart';

part 'notification_controller.dart';
part 'notification_state.dart';

final todoProvider =
    StateNotifierProvider<TodoController, TodoState>((ref) => TodoController());
final challangeProvider =
    StateNotifierProvider<ChallengeController, ChallengeState>(
        (ref) => ChallengeController());

final notificationProvider =
    StateNotifierProvider<NotificationController, NotificationState>(
        (ref) => NotificationController());
