part of prueba_vinnove.controllers;

class NotificationState {
  final List<NotificationSectionModel>? sections;

  NotificationState({this.sections});

  NotificationState copyWith({List<NotificationSectionModel>? sections}) =>
      NotificationState(sections: sections ?? this.sections);
}
