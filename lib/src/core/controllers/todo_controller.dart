part of prueba_vinnove.controllers;

class TodoController extends StateNotifier<TodoState> {
  TodoController() : super(TodoState(listOfTodos: []));

  void addTodo(String todo) {
    var result = state.listOfTodos ?? [];
    var newTodo = TodoModel(title: todo);
    result.add(newTodo);
    state = state.copyWith(listOfTodos: result);
  }

  void doneTask(int index) {
    state.listOfTodos?[index].hasDone = !state.listOfTodos![index].hasDone;
    state = state.copyWith(listOfTodos: state.listOfTodos ?? []);
  }

  void editTask(int index, String newTask) {
    state.listOfTodos?[index].title = newTask;
    state = state.copyWith(listOfTodos: state.listOfTodos ?? []);
  }

  void filterTasks() {
    var result =
        state.listOfTodos?.where((element) => element.hasDone).toList() ?? [];
    state = state.copyWith(filterTodos: result);
  }

  void removeTask(int index) {
    state.listOfTodos?.removeAt(index);
    state = state.copyWith(listOfTodos: state.listOfTodos ?? []);
  }
}
