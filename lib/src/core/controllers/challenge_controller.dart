part of prueba_vinnove.controllers;

class ChallengeController extends StateNotifier<ChallengeState> {
  ChallengeController() : super(ChallengeState(listOfOptions: listOfOptions));

  void selectOption(ChallengeOptions optionSelected) {
    state.listOfOptions
        ?.forEach((option) => option.isSelected = option == optionSelected);
    state = state.copyWith(listOfOptions: state.listOfOptions);
  }

  void answerQuestion(int numberQuestion, int value) {
    switch (numberQuestion) {
      case 1:
        state = state.copyWith(question1: value);

        return;
      case 2:
        state = state.copyWith(question2: value);
        return;
      case 3:
        state = state.copyWith(question3: value);
        return;
      case 4:
        state = state.copyWith(question4: value);
        return;
      case 5:
        state = state.copyWith(question5: value);
        return;
      default:
        throw ArgumentError('No existe esa pregunta: $numberQuestion');
    }
  }
}
