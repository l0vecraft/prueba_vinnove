part of prueba_vinnove.controllers;

class NotificationController extends StateNotifier<NotificationState>
    with DateHelper {
  NotificationController() : super(NotificationState(sections: []));

  void buildNotificationSection() async {
    var sortedDates = await sortNotifications(listNotifications);
    state = state.copyWith(sections: sortedDates);
  }
}
