part of prueba_vinnove.widgets;

class MyShaderPainter extends CustomPainter {
  final FragmentShader shader;
  final Duration? time;

  MyShaderPainter({required this.shader, this.time});
  @override
  void paint(Canvas canvas, Size size) {
    shader.setFloat(0, size.width);
    shader.setFloat(1, size.height);
    shader.setFloat(2, time?.inMilliseconds.toDouble() ?? 0);
    final Paint paint = Paint()..shader = shader;
    canvas.drawRect(Offset.zero & size, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
