part of prueba_vinnove.widgets;

class ScrollPainter extends CustomPainter {
  final double scrollPosition;
  final FragmentShader shader;

  ScrollPainter({
    super.repaint,
    required this.shader,
    required this.scrollPosition,
  });
  @override
  void paint(Canvas canvas, Size size) {
    shader.setFloat(0, size.width);
    shader.setFloat(1, size.height);
    shader.setFloat(2, scrollPosition);
    final Paint paint = Paint()..shader = shader;
    canvas.drawRect(Offset.zero & size, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
