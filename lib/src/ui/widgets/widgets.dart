library prueba_vinnove.widgets;

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shaders/flutter_shaders.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:prueba_vinnove/src/core/controllers/controllers.dart';
import 'package:prueba_vinnove/src/core/models/models.dart';
import 'package:prueba_vinnove/src/ui/styles/styles.dart';

part 'todo/todo_counter.dart';
part 'todo/todo_main_card.dart';
part 'todo/add_todo_button.dart';
part 'todo/todo_form_field.dart';
part 'todo/empty_todos.dart';
part 'todo/todo_card.dart';
part 'custom_button.dart';
part 'todo/todo_edit_dialog.dart';
part 'todo/todo_input_field.dart';
part 'todo/todo_content.dart';

part 'challenge/challenge_paragraph.dart';
part 'challenge/challenge_title.dart';
part 'challenge/challenge_card.dart';
part 'challenge/challenge_question.dart';
part 'challenge/circular_option.dart';
part 'challenge/challenge_tabs_section_result.dart';
part 'challenge/challenge_info_result_section.dart';
part 'challenge/challenge_result_score.dart';
part 'challenge/challenge_indicator_result.dart';
part 'challenge/challenge_bottom_result.dart';

part 'painters/my_shader_painter.dart';
part 'painters/scroll_painter.dart';

part 'shader/math_shader.dart';
part 'shader/scroll_shader.dart';
