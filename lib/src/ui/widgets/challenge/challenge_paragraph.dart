part of prueba_vinnove.widgets;

class ChallengeParagraph extends StatelessWidget {
  const ChallengeParagraph({
    required this.text,
    this.textAlign,
    super.key,
  });
  final String text;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: AppStyle.challengeText1,
      textAlign: textAlign,
    );
  }
}
