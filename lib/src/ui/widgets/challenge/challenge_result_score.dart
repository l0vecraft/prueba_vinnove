part of prueba_vinnove.widgets;

class ChallengeResultScore extends StatelessWidget {
  const ChallengeResultScore({
    super.key,
    required this.result,
  });

  final int result;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        SvgPicture.asset('assets/images/result.svg'),
        Positioned(
          right: 50.w,
          top: 10.h,
          child: Container(
            height: 48.h,
            width: 48.w,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                border: Border.all(color: Colors.blue, width: 3)),
            child: Center(
                child: Text(
              result.toString(),
              style: AppStyle.challengeTitle1.copyWith(color: Colors.blue),
            )),
          ),
        ),
      ],
    );
  }
}
