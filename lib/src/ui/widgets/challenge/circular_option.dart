part of prueba_vinnove.widgets;

class CircularOption extends StatefulWidget {
  CircularOption({required this.isSelected, super.key});
  final bool isSelected;

  @override
  State<CircularOption> createState() => _CircularOptionState();
}

class _CircularOptionState extends State<CircularOption> {
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      height: 22.h,
      width: 20.w,
      decoration: BoxDecoration(
          color: widget.isSelected ? Colors.blue : null,
          borderRadius: BorderRadius.circular(100),
          border: widget.isSelected
              ? null
              : Border.all(color: AppStyle.backgroundColor)),
      child: widget.isSelected
          ? Icon(
              Icons.check_rounded,
              size: 16.sp,
              color: Colors.white,
            )
          : null,
    );
  }
}
