part of prueba_vinnove.widgets;

class ChallengeQuestion extends ConsumerStatefulWidget {
  const ChallengeQuestion(
      {required this.title,
      required this.onPress,
      required this.isEnabled,
      super.key});

  final String title;
  final bool isEnabled;
  final Function(int value) onPress;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ChallengeQuestionState();
}

class _ChallengeQuestionState extends ConsumerState<ChallengeQuestion> {
  var _circularWidgets = List.generate(6, (index) => false);
  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: const Duration(milliseconds: 350),
      opacity: widget.isEnabled ? 1 : .3,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 15.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(widget.title,
                style: AppStyle.challengeText1
                    .copyWith(fontWeight: FontWeight.bold)),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 14.h, horizontal: 75.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: List.generate(
                    6,
                    (index) => GestureDetector(
                        onTap: () {
                          widget.onPress(index);
                          setState(() {
                            for (var i = 0; i < _circularWidgets.length; i++) {
                              _circularWidgets[i] = false;
                            }
                            _circularWidgets[index] = !_circularWidgets[index];
                          });
                        },
                        child: CircularOption(
                            isSelected: _circularWidgets[index]))),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("At no time",
                    style: AppStyle.challengeQuestionElementsText),
                Text("All the time",
                    style: AppStyle.challengeQuestionElementsText),
              ],
            )
          ],
        ),
      ),
    );
  }
}
