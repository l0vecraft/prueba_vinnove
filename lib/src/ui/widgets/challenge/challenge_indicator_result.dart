part of prueba_vinnove.widgets;

class ChallengeIndicatorResult extends StatelessWidget {
  const ChallengeIndicatorResult({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.h),
      child: SizedBox(
          width: 295.w,
          height: 112.h,
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: const BorderSide(
                  color: Colors.blue,
                  width: 2,
                )),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
              child: Row(
                children: [
                  const Expanded(
                    flex: 2,
                    child: ChallengeParagraph(
                        text:
                            "Your indicator will expire soon, next testing window opens in..."),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Text(
                          "11",
                          style: AppStyle.challengeBigNumber,
                        ),
                        const ChallengeParagraph(text: "days")
                      ],
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
