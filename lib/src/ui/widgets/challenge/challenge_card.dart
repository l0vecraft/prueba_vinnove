part of prueba_vinnove.widgets;

class ChallengeCard extends StatefulWidget {
  const ChallengeCard(
      {required this.title,
      required this.subtitle,
      required this.imagePath,
      required this.onPressed,
      this.isSelected = false,
      this.isLocked = false,
      super.key});
  final bool isSelected;
  final bool isLocked;
  final String title;
  final String subtitle;
  final String imagePath;
  final VoidCallback onPressed;
  @override
  State<ChallengeCard> createState() => _ChallengeCardState();
}

class _ChallengeCardState extends State<ChallengeCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.isLocked ? null : widget.onPressed,
      child: SizedBox(
        height: 129.h,
        width: 164.w,
        child: Card(
          color:
              widget.isLocked ? AppStyle.challengeDisableColor : Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: BorderSide(
                  color: widget.isSelected ? Colors.blue : Colors.grey)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(widget.title,
                          style: widget.isSelected
                              ? AppStyle.challengeSelectedCardText
                              : AppStyle.challengeCardTitleText),
                      Icon(
                        widget.isLocked
                            ? Icons.lock_rounded
                            : Icons.info_outline_rounded,
                        color: Colors.grey[800],
                      )
                    ],
                  ),
                ),
                Text(
                  widget.subtitle,
                  style: AppStyle.challengeCardSubtitleText,
                ),
                Center(child: SvgPicture.asset(widget.imagePath))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
