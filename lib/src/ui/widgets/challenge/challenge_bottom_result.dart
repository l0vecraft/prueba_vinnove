part of prueba_vinnove.widgets;

class ChallengeBottomResult extends StatelessWidget {
  const ChallengeBottomResult({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 60.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(width: 50.w),
          Expanded(
              child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 70.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(
                  4,
                  (index) => Container(
                        width: index == 0 ? 16 : 8,
                        height: 4,
                        decoration: BoxDecoration(
                            color: index == 0
                                ? AppStyle.challengePrimaryColor
                                : AppStyle.challengeDisableColor,
                            borderRadius: BorderRadius.circular(100)),
                      )).toList(),
            ),
          )),
          InkWell(
            onTap: () {},
            borderRadius: BorderRadius.circular(100),
            child: Container(
              height: 48.h,
              width: 48.w,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(color: AppStyle.challengePrimaryColor)),
              child: const Icon(
                Icons.arrow_forward_ios_rounded,
                color: AppStyle.challengePrimaryColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
