part of prueba_vinnove.widgets;

class ChallengeInfoResultSection extends StatelessWidget {
  const ChallengeInfoResultSection({
    required this.result,
    super.key,
  });
  final int result;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h),
      child: RichText(
          text: TextSpan(
              text: "Your score for wellbeing is ",
              style: AppStyle.challengeText1,
              children: [
            TextSpan(
                text: "$result.",
                style: AppStyle.challengeText1.copyWith(color: Colors.blue)),
            const TextSpan(
                text:
                    " The WHO-5 is a measure of quality of life, higher scores represent a better quality of life")
          ])),
    );
  }
}
