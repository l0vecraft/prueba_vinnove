part of prueba_vinnove.widgets;

class ChallenteTitle extends StatelessWidget {
  const ChallenteTitle({
    required this.text,
    super.key,
  });
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: AppStyle.challengeTitle1,
      textAlign: TextAlign.center,
    );
  }
}
