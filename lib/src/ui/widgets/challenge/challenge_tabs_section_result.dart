part of prueba_vinnove.widgets;

class ChallengeTabsSectionResult extends StatelessWidget {
  const ChallengeTabsSectionResult({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.h),
              child: Text(
                'Result',
                style: AppStyle.challengeTabSelectedText,
              ),
            ),
            Container(
              height: 3.h,
              width: 24.w,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: AppStyle.challengePrimaryColor),
            )
          ],
        ),
        Text('About Test', style: AppStyle.challengeTabText),
      ],
    );
  }
}
