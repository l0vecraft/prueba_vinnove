part of prueba_vinnove.widgets;

class TodoMainCard extends StatelessWidget {
  const TodoMainCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200.h,
      child: Card(
        color: AppStyle.backgroundColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: const BorderSide(color: AppStyle.ligthColor)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Tareas finalizadas', style: AppStyle.title1),
                    const Text(
                      'Sigue asi',
                      style: AppStyle.subTitle1,
                    ),
                  ],
                ),
              ),
              const TodoCounter()
            ],
          ),
        ),
      ),
    );
  }
}
