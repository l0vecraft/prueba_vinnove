part of prueba_vinnove.widgets;

class EmptyTodos extends StatelessWidget {
  const EmptyTodos({super.key, this.isFilter = false});
  final bool isFilter;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 30.h),
      child: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              isFilter
                  ? 'No se encontraron tareas filtradas'
                  : 'Aun no hay tareas, prueba creando unas',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppStyle.ligthColor,
                fontWeight: FontWeight.bold,
                fontSize: 25.sp,
              ),
            ),
            Icon(
              Icons.list_alt_rounded,
              size: 115.sp,
              color: AppStyle.ligthColor,
            )
          ],
        ),
      ),
    );
  }
}
