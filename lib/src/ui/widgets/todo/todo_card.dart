part of prueba_vinnove.widgets;

class TodoCard extends StatelessWidget {
  const TodoCard(
      {super.key,
      required this.title,
      required this.onDone,
      required this.onRemove,
      required this.onUpdate,
      this.hasDone = false});

  final String title;
  final VoidCallback onUpdate;
  final VoidCallback onRemove;
  final VoidCallback onDone;
  final bool hasDone;

  @override
  Widget build(BuildContext context) {
    return FadeInLeft(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.h),
        child: SizedBox(
          height: 100.h,
          child: Card(
            color: AppStyle.secondaryColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: const BorderSide(color: AppStyle.ligthColor)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        child: InkWell(
                          onTap: onDone,
                          borderRadius: BorderRadius.circular(100),
                          child: AnimatedContainer(
                            duration: const Duration(milliseconds: 300),
                            height: 30.h,
                            width: 30.w,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                border: Border.all(
                                    color: hasDone
                                        ? Colors.transparent
                                        : AppStyle.primaryColor),
                                color: hasDone
                                    ? Colors.green
                                    : Colors.transparent),
                          ),
                        ),
                      ),
                      Text(
                        title,
                        style: AppStyle.todoItemStyle(hasDone),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    if (!hasDone)
                      IconButton(
                          onPressed: onUpdate,
                          icon: Icon(
                            Icons.mode_edit_outline_outlined,
                            color: AppStyle.ligthColor,
                            size: 25.sp,
                          )),
                    IconButton(
                        onPressed: onRemove,
                        icon: Icon(
                          Icons.delete_outline_rounded,
                          color: AppStyle.ligthColor,
                          size: 25.sp,
                        ))
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
