part of prueba_vinnove.widgets;

class TodoEditDialog extends ConsumerStatefulWidget {
  const TodoEditDialog({required this.value, required this.index, Key? key})
      : super(key: key);

  final String value;
  final int index;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TodoEditDialogState();
}

class _TodoEditDialogState extends ConsumerState<TodoEditDialog> {
  late TextEditingController _controller;
  final _focusTextInput = FocusNode();

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.value);
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = ref.read(todoProvider.notifier);
    return GestureDetector(
      onTap: () {
        if (_focusTextInput.hasFocus) {
          _focusTextInput.unfocus();
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 26.w),
        color: AppStyle.backgroundColor,
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25.h),
            child: Text(
              'Editar Tarea',
              textAlign: TextAlign.center,
              style: AppStyle.title1,
            ),
          ),
          TodoInputField(
            focusTextInput: _focusTextInput,
            controller: _controller,
            message: "Agregar una tarea",
          ),
          SizedBox(height: 90.h),
          CustomButton(
            onPress: () => Navigator.pop(context),
            text: 'Cancelar',
            borderColor: AppStyle.ligthColor,
          ),
          SizedBox(height: 20.h),
          CustomButton(
            text: "Confirmar",
            onPress: () {
              if (_controller.text.isNotEmpty) {
                viewModel.editTask(widget.index, _controller.text);
                Navigator.pop(context);
              }
            },
          ),
        ]),
      ),
    );
  }
}
