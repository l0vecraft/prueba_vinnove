part of prueba_vinnove.widgets;

class TodoContent extends ConsumerWidget {
  const TodoContent(
      {required this.listOfTodos, this.isFilter = false, super.key});
  final List<TodoModel>? listOfTodos;
  final bool isFilter;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final viewModel = ref.read(todoProvider.notifier);
    return Column(
      children: listOfTodos
              ?.map((todo) => TodoCard(
                    title: todo.title,
                    hasDone: todo.hasDone,
                    onDone: () => isFilter
                        ? null
                        : viewModel.doneTask(listOfTodos?.indexOf(todo) ?? 0),
                    onRemove: () =>
                        viewModel.removeTask(listOfTodos?.indexOf(todo) ?? 0),
                    onUpdate: () {
                      if (!todo.hasDone) {
                        showModalBottomSheet(
                          context: context,
                          backgroundColor: AppStyle.backgroundColor,
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                          )),
                          showDragHandle: true,
                          builder: (context) => TodoEditDialog(
                            value: todo.title,
                            index: listOfTodos?.indexOf(todo) ?? 0,
                          ),
                        );
                      }
                    },
                  ))
              .toList() ??
          [],
    );
  }
}
