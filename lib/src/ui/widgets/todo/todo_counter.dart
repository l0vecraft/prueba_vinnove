part of prueba_vinnove.widgets;

class TodoCounter extends ConsumerStatefulWidget {
  const TodoCounter({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TodoCounterState();
}

class _TodoCounterState extends ConsumerState<TodoCounter> {
  @override
  Widget build(BuildContext context) {
    final state = ref.watch(todoProvider);
    return Container(
      height: 130.h,
      width: 120.w,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: AppStyle.primaryColor),
      child: Center(
          child: Text(
        "${state.todosDone}/${state.listOfTodos?.length}",
        style: AppStyle.counterStyle,
      )),
    );
  }
}
