part of prueba_vinnove.widgets;

class TodoInputField extends StatelessWidget {
  const TodoInputField({
    super.key,
    required this.message,
    required FocusNode focusTextInput,
    required TextEditingController? controller,
    this.onSubmit,
  })  : _focusTextInput = focusTextInput,
        _controller = controller;

  final FocusNode _focusTextInput;
  final TextEditingController? _controller;
  final String message;
  final Function? onSubmit;

  @override
  Widget build(BuildContext context) {
    return TextField(
      focusNode: _focusTextInput,
      controller: _controller,
      cursorColor: AppStyle.ligthColor,
      style: AppStyle.textFieldStyle,
      decoration: InputDecoration(
          hintText: message,
          hintStyle: AppStyle.textFieldStyle,
          filled: true,
          fillColor: AppStyle.secondaryColor,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(color: AppStyle.ligthColor))),
      onSubmitted: (value) {
        if (onSubmit != null) onSubmit!(value);
      },
    );
  }
}
