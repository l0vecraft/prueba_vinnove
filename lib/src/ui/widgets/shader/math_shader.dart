part of prueba_vinnove.widgets;

class MathShader extends StatefulWidget {
  const MathShader({super.key});

  @override
  State<MathShader> createState() => _MathShaderState();
}

class _MathShaderState extends State<MathShader>
    with SingleTickerProviderStateMixin {
  late Ticker _ticker;
  var _currentTime = Duration.zero;

  @override
  void initState() {
    super.initState();
    _ticker = createTicker((elapsed) {
      setState(() {
        _currentTime = elapsed;
      });
    });
    _ticker.start();
  }

  @override
  void dispose() {
    _ticker.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => ShaderBuilder(
      (context, shader, _) => CustomPaint(
          size: MediaQuery.of(context).size,
          painter: MyShaderPainter(shader: shader, time: _currentTime)),
      assetKey: 'shaders/myshader.frag');
}
