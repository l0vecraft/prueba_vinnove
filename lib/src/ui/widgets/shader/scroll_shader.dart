part of prueba_vinnove.widgets;

class ScrollShader extends StatefulWidget {
  const ScrollShader({super.key});

  @override
  State<ScrollShader> createState() => _ScrollShaderState();
}

class _ScrollShaderState extends State<ScrollShader> {
  final _scrollController = ScrollController();
  final _elements = List.generate(50, (index) => 'elemento $index');
  double _currentPosition = 1.0;
  var x = [
    Color.fromRGBO(127, 116, 228, 1),
    Color.fromRGBO(199, 95, 251, 1),
    Color.fromRGBO(237, 51, 206, 1),
    Color.fromRGBO(7, 177, 187, 1),
    Color.fromRGBO(51, 111, 229, 1),
    Color.fromRGBO(223, 112, 244, 1),
  ];

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      setState(() {
        _currentPosition = (_scrollController.position.pixels / 100);
      });
    });
  }

  @override
  Widget build(BuildContext context) => ShaderBuilder(
        assetKey: 'shaders/scroll_shader.frag',
        (context, shader, _) => Center(
          child: CustomPaint(
            size: MediaQuery.of(context).size,
            painter:
                ScrollPainter(shader: shader, scrollPosition: _currentPosition),
            child: Container(
              height: 220.h,
              child: ListView.builder(
                  controller: _scrollController,
                  itemCount: _elements.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => Container(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(bottom: 10.h),
                                child: Text(
                                  'Elemento $index',
                                  style: TextStyle(fontSize: 24.sp),
                                ),
                              ),
                              const CircleAvatar(backgroundColor: Colors.white)
                            ],
                          ),
                        ),
                      )),
            ),
          ),
        ),
      );
}
