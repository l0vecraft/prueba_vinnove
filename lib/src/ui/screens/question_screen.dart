part of prueba_vinnove.screens;

class QuestionScreen extends ConsumerStatefulWidget {
  const QuestionScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends ConsumerState<QuestionScreen>
    with TickerProviderStateMixin {
  late AnimationController _controller1, _controller2;
  late Animation<double> _animation1, _animation2;

  @override
  void initState() {
    super.initState();
    _controller1 = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    _controller2 = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    _animation1 = Tween<double>(begin: 0, end: 1).animate(
        CurvedAnimation(parent: _controller1, curve: Curves.easeInOut));
    _animation2 = Tween<double>(begin: 0, end: 1).animate(
        CurvedAnimation(parent: _controller2, curve: Curves.easeInOut));

    _controller1.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _controller1.dispose();
    _controller2.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = ref.read(challangeProvider.notifier);
    final state = ref.watch(challangeProvider);

    return CupertinoFullscreenDialogTransition(
      primaryRouteAnimation: _animation1,
      secondaryRouteAnimation: _animation2,
      linearTransition: true,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: const SizedBox(),
          title: Text(
            "Wellbeing",
            style: AppStyle.challengeTitleAppBar,
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(top: 18.h, right: 16.w),
              child: Text("Aa", style: AppStyle.challengeTitle1),
            ),
            InkWell(
              onTap: () => Navigator.pop(context),
              child: Padding(
                padding: EdgeInsets.only(top: 23.h, right: 10.w),
                child: Text("Close",
                    style: AppStyle.challengeTitleAppBar
                        .copyWith(color: Colors.blue, fontSize: 14.sp)),
              ),
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    const ChallengeParagraph(
                        text:
                            "Please respond to each item by marking one box per row."),
                    ChallengeQuestion(
                      title: "I have felt cheerful in good spirits",
                      isEnabled: true,
                      onPress: (value) => viewModel.answerQuestion(1, value),
                    ),
                    ChallengeQuestion(
                      title: "I have felt calm and relaxed",
                      isEnabled: state.question1 != null,
                      onPress: (value) => viewModel.answerQuestion(2, value),
                    ),
                    ChallengeQuestion(
                      title: "I have felt active and vigorous",
                      isEnabled: state.question2 != null,
                      onPress: (value) => viewModel.answerQuestion(3, value),
                    ),
                    ChallengeQuestion(
                      title: "I woke up feeling fresh and rested",
                      isEnabled: state.question3 != null,
                      onPress: (value) => viewModel.answerQuestion(4, value),
                    ),
                    ChallengeQuestion(
                      title:
                          "My daily life has been filled with things that interest me",
                      isEnabled: state.question4 != null,
                      onPress: (value) => viewModel.answerQuestion(5, value),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.h),
                      child: CustomButton(
                        onPress: state.isEnabledContinueButton
                            ? () => Navigator.pushNamed(context, 'result')
                            : null,
                        text: "Continue",
                        color: AppStyle.challengePrimaryColor,
                        disabledColor: AppStyle.challengeDisableColor,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
