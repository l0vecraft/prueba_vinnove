part of prueba_vinnove.screens;

class ShaderScreen extends ConsumerStatefulWidget {
  const ShaderScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ShaderScreenState();
}

class _ShaderScreenState extends ConsumerState<ShaderScreen> {
  // void loadFragment() async {
  //   var program = await FragmentProgram.fromAsset('shaders/myshader.frag');
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: const ScrollShader(),
    );
  }
}
