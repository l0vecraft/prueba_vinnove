part of prueba_vinnove.screens;

class ChallengeScreen extends ConsumerStatefulWidget {
  const ChallengeScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ChallengeScreenState();
}

class _ChallengeScreenState extends ConsumerState<ChallengeScreen> {
  @override
  Widget build(BuildContext context) {
    var viewModel = ref.read(challangeProvider.notifier);
    var state = ref.watch(challangeProvider);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          onPressed: () => context.pop(true),
          icon: const Icon(Icons.arrow_back_ios_new_rounded,
              color: AppStyle.backgroundColor),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: List.generate(
              4,
              (index) => Padding(
                    padding: EdgeInsets.symmetric(horizontal: 2.5.w),
                    child: Container(
                      height: 6.h,
                      width: 18.w,
                      decoration: BoxDecoration(
                          color: AppStyle.challengePrimaryColor,
                          borderRadius: BorderRadius.circular(100)),
                    ),
                  )),
        ),
        actions: [
          Padding(
              padding: const EdgeInsets.all(18.0),
              child: Text("4/4", style: AppStyle.challengeStepIndicatorStyle))
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        child: CustomScrollView(
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5.h),
                    child: const ChallenteTitle(text: 'Well-being Check-up'),
                  ),
                  const ChallengeParagraph(
                      text:
                          'You can choose one or more of the following to scan your wellbeing temperature. Select all three for a complete wellbeing profile.'),
                  SizedBox(height: 16.h)
                ],
              ),
            ),
            SliverGrid.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: .8, crossAxisCount: 2),
              itemCount: state.listOfOptions?.length,
              itemBuilder: (context, index) => ChallengeCard(
                imagePath: state.listOfOptions?[index].imagePath ?? '',
                subtitle: state.listOfOptions?[index].subtitle ?? '',
                title: state.listOfOptions?[index].title ?? '',
                isLocked: state.listOfOptions?[index].isBlocked ?? false,
                isSelected: state.listOfOptions?[index].isSelected ?? false,
                onPressed: () {
                  viewModel.selectOption(state.listOfOptions![index]);
                },
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(top: 18.h),
                child: CustomButton(
                    color: AppStyle.challengePrimaryColor,
                    onPress: state.listOfOptions
                                ?.any((option) => option.isSelected = true) ??
                            false
                        ? () => Navigator.pushNamed(context, 'question')
                        : () {},
                    text: "Continue"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
