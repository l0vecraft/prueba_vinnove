part of prueba_vinnove.screens;

class NotificationScreen extends ConsumerStatefulWidget {
  const NotificationScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _NotificationScreenState();
}

class _NotificationScreenState extends ConsumerState<NotificationScreen> {
  @override
  void initState() {
    super.initState();
    ref.read(notificationProvider.notifier).buildNotificationSection();
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(notificationProvider);
    return Scaffold(
        appBar: AppBar(title: const Text("Notificaciones")),
        body: state.sections!.isEmpty || state.sections == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                shrinkWrap: true,
                itemCount: state.sections?.length ?? 0,
                itemBuilder: (context, index) => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            state.sections?[index].title ?? '',
                            style: TextStyle(fontSize: 20.sp),
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const ClampingScrollPhysics(),
                          itemCount:
                              state.sections?[index].notifications.length,
                          itemBuilder: (context, jindex) => ListTile(
                            title: Text(state.sections?[index]
                                    .notifications[jindex].title ??
                                ''),
                          ),
                        )
                      ],
                    )));
  }
}
