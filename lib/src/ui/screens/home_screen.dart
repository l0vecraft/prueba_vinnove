part of prueba_vinnove.screens;

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Prueba vinnove'),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 30.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton(
                  onPressed: () => context.go('/todo'),
                  child: const Text('Todo app')),
              ElevatedButton(
                  onPressed: () => context.go('/challenge'),
                  child: const Text('Challenge app')),
              ElevatedButton(
                  onPressed: () => context.push('/notifications'),
                  child: const Text('Notification screen')),
              ElevatedButton(
                  onPressed: () => context.go('/shaders'),
                  child: const Text('Fragments screen'))
            ],
          ),
        ),
      ),
    );
  }
}
