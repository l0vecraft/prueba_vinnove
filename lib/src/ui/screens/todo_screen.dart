part of prueba_vinnove.screens;

class TodoScreen extends ConsumerStatefulWidget {
  const TodoScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TodoScreenState();
}

class _TodoScreenState extends ConsumerState<TodoScreen> {
  final _focusTextInput = FocusNode();
  var _didFilter = false;

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(todoProvider);
    final viewModel = ref.read(todoProvider.notifier);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo app'),
        centerTitle: true,
        backgroundColor: AppStyle.primaryColor,
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  _didFilter = !_didFilter;
                });
                viewModel.filterTasks();
              },
              icon: Icon(_didFilter
                  ? Icons.filter_list_off_rounded
                  : Icons.filter_list_rounded))
        ],
      ),
      backgroundColor: AppStyle.backgroundColor,
      body: GestureDetector(
        onTap: () {
          if (_focusTextInput.hasFocus) {
            _focusTextInput.unfocus();
          }
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
          child: CustomScrollView(slivers: [
            SliverList(
              delegate: SliverChildListDelegate([
                const TodoMainCard(),
                SizedBox(height: 15.h),
                TodoFormField(focus: _focusTextInput),
                SizedBox(height: 20.h),
                if (state.listOfTodos == null ||
                    (state.listOfTodos?.isEmpty ?? true)) ...<Widget>[
                  const EmptyTodos()
                ] else ...[
                  TodoContent(
                      isFilter: _didFilter,
                      listOfTodos:
                          _didFilter ? state.filterTodos : state.listOfTodos)
                ]
              ]),
            ),
          ]),
        ),
      ),
    );
  }
}
