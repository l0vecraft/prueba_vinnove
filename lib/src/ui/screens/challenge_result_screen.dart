part of prueba_vinnove.screens;

class ChallangeResultScreen extends ConsumerWidget {
  const ChallangeResultScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(challangeProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "TestResult",
          style: AppStyle.challengeTitleAppBar
              .copyWith(fontWeight: FontWeight.bold),
        ),
        leading: const SizedBox(),
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          SvgPicture.asset('assets/images/vector.svg'),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: SvgPicture.asset('assets/images/file.svg'),
          ),
        ],
      ),
      body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildListDelegate([
                  Text(
                    "Wellbeing",
                    textAlign: TextAlign.center,
                    style: AppStyle.challengeTitleAppBar
                        .copyWith(color: AppStyle.challengePrimaryColor),
                  ),
                  SizedBox(height: 25.h),
                  ChallengeResultScore(result: state.result),
                  SizedBox(height: 16.h),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.h),
                    child: const ChallengeTabsSectionResult(),
                  ),
                  ChallengeInfoResultSection(result: state.result),
                  const ChallengeParagraph(
                      text: "For more information, check about test",
                      textAlign: TextAlign.center),
                  const ChallengeIndicatorResult(),
                  const ChallengeParagraph(
                      text:
                          "Your privacy is important to us, and your information is stored anonymously",
                      textAlign: TextAlign.center),
                ]),
              ),
              const SliverToBoxAdapter(
                child: ChallengeBottomResult(),
              )
            ],
          )),
    );
  }
}
