library prueba_vinnove.screens;

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shaders/flutter_shaders.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:prueba_vinnove/src/core/controllers/controllers.dart';
import 'package:prueba_vinnove/src/ui/styles/styles.dart';

import '../widgets/widgets.dart';

part 'challenge_screen.dart';
part 'challenge_result_screen.dart';
part 'home_screen.dart';
part 'todo_screen.dart';
part 'question_screen.dart';
part 'notification_screen.dart';
part 'shader_screen.dart';
